
var db = firebase.apps[0].firestore();
var tabla = document.getElementById('tabla');

//Leer documentos
db.collection("datosPersonales").get().then((document) => {
    tabla.innerHTML = '';
    document.forEach((item) => {
        tabla.innerHTML += `<tr>
            <td> ${item.id} </td>
            <td> ${item.data().idCard} </td>
            <td> ${item.data().firstname} </td>
            <td> ${item.data().lastname} </td>
            <td><div class="d-flex justify-content-center">
            <button class="btn btn-primary mr-3" onclick="actualizar('${item.id}');">Editar</button>
            <button class="btn btn-danger" onclick="borrar('${item.id}');" >Borrar</button>
          </div> </td>
            </tr>`;
    });
}).catch((error) => {
    console.log("Error getting document:", error);
});



function borrar(identificador) {
    db.collection("datosPersonales").doc(identificador).delete().then(() => {
        console.log("Document successfully deleted!");
        window.location = 'index.html';
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
}


function actualizar(identificador) {
    window.location = `inspersona.html?Id=${identificador}`;
}