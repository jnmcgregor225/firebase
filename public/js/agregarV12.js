
var db = firebase.apps[0].firestore();
const txtIdCard = document.querySelector('#txtidcard');
const txtFirstname = document.querySelector('#txtfirstname');
const txtLastname = document.querySelector('#txtlastname');
const btnInsert = document.querySelector('#btnInsert');

var isUpdate = false;
var primaryKey = '';

btnInsert.addEventListener('click', function () {

    if (!isUpdate) {
        db.collection("datosPersonales").add({
            "idCard": parseInt(txtIdCard.value, 10),
            "firstname": txtFirstname.value,
            "lastname": txtLastname.value
        })
            .then(function (docRef) {
                var mensaje = "Document written with ID: " + docRef.id
                // alert(mensaje);
                limpiar();
            })
            .catch(function (FirebaseError) {
                var mensaje = "Error adding document: " + FirebaseError
                alert(mensaje);
            });
    } else {
        db.collection("datosPersonales").doc(primaryKey).update({
            "idCard": parseInt(txtIdCard.value, 10),
            "firstname": txtFirstname.value,
            "lastname": txtLastname.value
        })
            .then(function (docRef) {
                // var mensaje = "Document updating with ID: " + docRef.id
                // alert(mensaje);
                limpiar();
            })
            .catch(function (FirebaseError) {
                var mensaje = "Error updating document: " + FirebaseError
                alert(mensaje);
            });
    }
});

function limpiar() {
    txtIdCard.value = '';
    txtFirstname.value = '';
    txtLastname.value = '';
    window.location.href = 'index.html';
    // txtIdCard.focus();
}

window.onload = function () {
    var url_string = window.location.href
    var url = new URL(url_string);
    primaryKey = url.searchParams.get("Id");

    if (primaryKey != null && primaryKey != undefined && primaryKey != '') {
        isUpdate = true;
        db.collection("datosPersonales").doc(primaryKey).get().then((doc) => {
            txtIdCard.value = doc.data().idCard;
            txtFirstname.value = doc.data().firstname;
            txtLastname.value = doc.data().lastname;
        }).catch((error) => {
            console.log("Error getting document:", error);
        });
    }

}